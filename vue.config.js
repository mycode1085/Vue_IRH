const path = require("path");
const pxtorem = require("postcss-pxtorem");
const autoprefixer = require("autoprefixer");

const resolve = file => path.resolve(__dirname, file);

//获取多页面配置 
const getPages=require('./src/utils/getPages.js');
let pages=getPages.pages();

module.exports = {
	pages,
	outputDir: "dist",
	assetsDir: "static",
	publicPath: process.env.VUE_BASE_PATH,     
  lintOnSave:false,
  devServer: {
    overlay:{
      warning:false,
      errors:false
  },
    proxy: {
      '/': {
				//target: 'http://222.186.174.9:13163/api',
				//target: 'http://39.97.123.63:10900/api',
				target: 'https://www.imuster.top/test/api',
        secure: true, // false为http访问，true为https访问
        changeOrigin: true, // 跨域访问设置，true代表跨域
        ws: false,   //  如果要代理 websockets，配置这个参数
      },
      // '/upload': {
      //   //target: 'http://106.13.141.241/mock/44/',
			// 	target: 'http://39.105.0.169:8080/',
      //   secure: false, // false为http访问，true为https访问
      //   changeOrigin: true, // 跨域访问设置，true代表跨域
      //   ws: true,   //  如果要代理 websockets，配置这个参数
      //   pathRewrite: {
      //     '^/upload':''
      //   }
      // }
    }
  },


	css: {
		loaderOptions: {
			postcss: {
				plugins: [
					autoprefixer(),
					pxtorem({
						rootValue: 16,
						propList: ["*"],
						selectorBlackList: ["ig-"],
						unitPrecision: 5,
						replace: true,
						mediaQuery: false,
						minPixelValue: 2
					})
				]
			},
			less: {
				modifyVars: {
					'@color':"aquamarine"
				}
			},
		}
	},
	chainWebpack: config => {
		config.resolve.alias
			.set("@", resolve("src"))
			.set("@assets", resolve("src/assets"))
			.set("@components", resolve("src/components"))
			.set("@views", resolve("src/views"))
			.set("@utils", resolve("src/utils"))
			.set("@config", resolve("src/config"));
	}
	
	
};
