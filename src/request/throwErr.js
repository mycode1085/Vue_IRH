// axios  捕错
export const  throwErr = (code, response) => {
  let message = '请求错误'
  switch (code) {
    case 401:
      message = '禁止访问'
      break
    case 402:
      message = '身份验证已经过期,请重新登陆'
      break
    case 100:
      message = `非法参数`
      break
    case 500:
      message = '服务器内部错误'
      break
    default:
      message = response.data
  }
  return message
}