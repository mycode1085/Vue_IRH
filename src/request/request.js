import axios from 'axios'
//import { throwErr } from '@/utils' //utils 捕捉服务端http状态码的方法
//import store from '../store'   //引入vuex的相关操作
// import { Message} from 'element-ui' //element Toast的提示
// import router from 'vue-router'
import QS from 'qs'
import { throwErr } from './throwErr.js'    //在这个文件里面捕获错误
// element  ui  的加载效果
// import { showFullScreenLoading, tryHideFullScreenLoading } from './loading/loading.js'

// 
// import Nprogress from 'nprogress'
// import 'nprogress/nprogress.css'
// 在请求拦截器里面使用  Nprogress.start()   展示进度条
// 在响应拦截器里面使用   Nprogress.done()   隐藏进度条

//过滤请求   请求拦截器
axios.interceptors.request.use(config => {
  // 打开加载条
  // Nprogress.start()
  //config 为请求的一些配置 例如：请求头 请求时间 Token  可以根据自己的项目需求个性化配置，参考axios的中文说明手册  自己多动动手
 //由于我们项目的后端大大给力，很多东西在服务端帮我们处理好了所以请求阶段只要传好参数就好了
  
  // 需要设置请求头
  /* 给请求头里面添加一个属性 Authorization 并将其值设置为token令牌*/
  //todo
  // console.log();
  config.headers.Authorization = (window.localStorage.getItem('token')) || ''
  config.headers.userAccessToken = window.localStorage.getItem('cookie') || ''
  // config.headers.Authorization = 'asdfasdf'
  // config.headers.post['Content-Type'] = 'application/json'
  // console.log('confing',config);
  // config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
  config.timeout = 10 * 1000 //请求响应时间  10s
  return config
}, error => {
  return Promise.reject(error)
})
// 添加响应拦截器
axios.interceptors.response.use(
  response => {
    // 关闭加载条**************
    // Nprogress.done()
    if (response.data.code === 200) {   //服务端定义的响应code码为200时请求成功
      // if()
      // window.localStorage.setItem('token',response.data.text)
      return Promise.resolve(response.data) //使用Promise.resolve 正常响应
    } else if (response.data.code !== 200) { //服务端定义的响应code码为其他时为未登录
      // 这个Vuex还需要再学习一下
      // store.dispatch('setUserInfo', {})
      // Message({
      //   message: '请求失败'
      // })
      // 这个可以用于强制跳转到登陆页面
      // router.push('/login')
      return Promise.reject(response.data)    //使用Promise.reject 抛出错误和异常
    } else {
      return Promise.reject(response.data)
    }
  },
  error => {
    // 关闭加载条**************
    // tryHideFullScreenLoading()
    if (error && error.response) {
      let res = {}
      res.code = error.response.code
      res.msg = throwErr(error.response.code, error.response) //throwErr 捕捉服务端的http状态码 
      return Promise.reject(res)
    }
    return Promise.reject(error)
  }
)
export default function request(method, url, data) {  //暴露 request 给我们好API 管理
  method = method.toLocaleLowerCase()   //封装RESTful API的各种请求方式 以 post get delete为例
  if (method === 'post') {
    return axios.post(url, data)    //axios的post 默认转化为json格式
  } else if (method === 'get') {     
    return axios.get(url, {
      params: data
    })
  } else if (method === 'delete') {
    return axios.delete(url, {
      params: data
    })
  } else if (method === 'put') {
    return axios.put(url, data)
  } else if (method === 'form') {
    return axios.post(url, QS.stringify(data))
  }
}


