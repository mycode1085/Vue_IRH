import request from '../request.js'
import axios from 'axios' //引入axios的封装方法

//获得已经通过审核的申请
export const getNewestApplyInfo = () => {
  return request("get", "/order/donation")
}

//获得已经发放资金的申请
export const getFinishApplyInfoList = (pageSize, currentPage) => {
  return request("get", `/order/donation/finish/${pageSize}/${currentPage}`)
}


//获得正在审核的申请
export const getApplyingInfoList = (pageSize, currentPage) => {
  return request("get", `/order/donation/unfinish/${pageSize}/${currentPage}`)
}

// 根据id查询公益详情  type 5 申请已完成  2 正在审核
export const getDonationDetail = (type, id)=> {
  return request('get',`/order/donation/detail/${type}/${id}`)
}
// 用户给公益提供自己的态度 type 1 不同意 2 同意
export const getDonationDetail_attribute = (type, targetId)=> {
  return request('get',`/order/donation/attribute/${type}/${targetId}`)
}
// 根据作者id获得作者的昵称
export const getUserInfoById = (userid) => {
  return request('get',`/user/user/${userid}`)
}

//根据id查看申请信息  type 5-已完成的申请   2-未完成的申请
export const getApplyInfoById = (id, type)=> {
  return request("get", `/order/donation/detail/${type}/${id}`)
}