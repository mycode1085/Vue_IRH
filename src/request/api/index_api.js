import request from '../request.js'
import axios from 'axios' //引入axios的封装方法


// 热搜总榜接口
export const get_forum_hot_topic = (pageSize, currentPage) => {
  return request('get',`/life/hot/total/${pageSize}/${currentPage}`)
}

// 实时热搜
export const getForumCurrentHotTopic = (pageSize, currentPage) => {
  return request("get", `/life/hot/current/${pageSize}/${currentPage}`)
}

// 获取广告 和学校通知  type 取1广告  2学校通知
export const get_propagate = (pageSize,currentPage,type) => {
  return request('get',`/user/propagate/${pageSize}/${currentPage}/${type}`)
}
// 根据广告和学校通知的id获取详情
export const get_propagate_detail = (id) => {
  return request('get',`/user/propagate/detail/${id}`)
}

//获得已经转账的公益基金申请
export const getFinishDonation = (pageSize, currentPage) => {
  return request('get',`/order/donation/finish/${pageSize}/${currentPage}`)
}
// 根据id查询公益详情  type 5 申请已完成  2 正在审核
export const getDonationDetail = (type, id)=> {
  return request('get',`/order/donation/detail/${type}/${id}`)
}

// 用户给公益提供自己的态度 type 1 不同意 2 同意
export const getDonationDetail_attribute = (type, targetId)=> {
  return request('get',`/order/donation/attribute/${type}/${targetId}`)
}
//typ取值5-标识查看的申请已经完成，返回的信息中需要包含使用了哪些人的订单   2-标识查看正在审核的或者审核通过但是未转账的
export const getDonationDetailById = (id, typ) => {
  return request('get',`/order/donation/detail/${typ}/${id}`)
}