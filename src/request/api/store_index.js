import request from '../request.js'
import axios from 'axios' //引入axios的封装方法


// 首页的推荐  商品
export const get_recommend_offline = (pageSize,currentPage) => {
  return request('get',`/goods/recommend/realtime/${pageSize}/${currentPage}
  `)
}

// /goods/recommend/demand/{pageSize}/{currentPage}
// 首页的推荐 需求
export const get_demand_recommendOffline = (pageSize,currentPage) => {
  return request('get',`/goods/recommend/demand/${pageSize}/${currentPage}
  `)
}
// 商城轮播图   获取轮播图

export const get_swipers = (pageSize,currentPage) => {
  return request('get',`/goods/rotation/${pageSize}/${currentPage}`)
}

// 记录轮播图点击
export const get_add_swipers_history = (id) => {
  return request('get',`/goods/rotation/browse/${id}`)
}
