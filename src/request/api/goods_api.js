import request from '../request.js'
import axios from 'axios'

//获得个人发布的需求
export const getOwnerDemandPage = (pageSize, currentPage) => {
  return request("get", `/goods/goods/demand/list/${pageSize}/${currentPage}`)
}

export const deleteDemandById = (id) => {
  return request("delete", `/goods/goods/demand/${id}`)
}

//用户查看自己的订单列表   type取值: 1-作为买家  2-作为卖家
export const getOrderListByType = (pageSize, currentPage, type) => {
  return request("get", `/order/order/es/list/${type}/${pageSize}/${currentPage}`)
}

//根据商品id获得商品的简略信息
export const getProductBriefById = (productId) => {
  //goods/goods/es/brief/1
  return request("get", `/goods/goods/es/brief/${productId}`)
}

export const finishOrder = (orderId) => {
  return request("get", `/order/order/es/finish/${orderId}`)
}

export const getMyReleaseGoodsByPage = (pageSize, currentPage) => {
  return request("get", `/goods/goods/es/list/${pageSize}/${currentPage}`)
}


// 根据作者id获得作者的昵称
export const getUserInfoById = (userid) => {
  return request('get',`/user/user/${userid}`)
}

export const getGoodsListByUserId = (userId, pageSize, currentPage) =>{
  return request("get", `/goods/goods/es/user/${pageSize}/${currentPage}/${userId}`)
}

//发布二手商品
export const releaseProduct = (productInfo) => {
  return request("put", "/goods/goods/es", productInfo)
}

//获得商品分类
export const goodsCategory = ()=>{
  return request("get", "/goods/goods/category/tree")
}



//根据用户id获得用户的需求列表
export const getDemandListByUserId = (userId, pageSize, currentPage) =>{
  return request("get", `/goods/goods/demand/user/${pageSize}/${currentPage}/${userId}`)
}