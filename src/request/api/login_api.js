import request from '../request.js'
import axios from 'axios' //引入axios的封装方法

// export const login = (param)=> {
//   return request("post", "/security/login", param)
// }


// 账号注册
export const post_register = (code,params) => {
  return request('post',`/user/user/register/${code}`,params)
}
// 判断用户的用户名和邮箱是否唯一   params {type:"email",validValue:'1111@qq.com' }  还可以phoneNum,nickname,alipayNum 
export const checkData = (params) => {
  return request('post',"/user/user/check", params)
}

// 账号密码登陆  // email password
export const post_login = (params) => {
  return request('form',`/security/password`,params)
}
// 登陆验证  验证码 登陆位置
export const get_login_code = (email) => {
  return request('get',`/security/sendCode/${email}`)
}
// 验证码登陆 loginName     emailVerifyCode 
export const post_emailCodeLogin = (params) => {
  return request('form',`/security/emailCodeLogin`,params)
}

// 找回密码 params中 code   password  email
export const post_resetPwd = (params) => {
  return request('post',`/user/user/resetPwd`,params)
}
// 注册  
// 发送邮箱验证码   type  1注册 2登陆 3找回密码
export const post_ToEmailCode = (type,email) => {
  return request('get',`/security/sendCode/${type}/${email}`)
}