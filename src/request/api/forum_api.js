import request from '../request.js'
import axios from 'axios' //引入axios的封装方法

// 暴露接口  在用户写文章的时候选择的标签  获得所有的标签
export const get_forum_all_tag = (params) => {
  return request('get','/life/forum/tag',params)    //获得所有的标签
} 

// 暴露一个获得自己文章的接口  
export const get_forum_myArticle = (params) => {
  return request('post','/life/forum/article/list',params)
}
// 暴露一个用户写留言的接口
export const write_forum_reply = (params) => {
  return request('post','/life/forum/review/write',params)
}
// 用户分页查看自己的收藏列表
export const get_collect_article = (params) => {
  return request('post','/life/forum/collect',params)
}
// 根据文章的id   获得文章的简略信息
export const get_collect_article_brief = (id) => {
  return request('get',`/life/forum/article/brief/${id}`)
}
// 根据作者id获得作者的昵称
export const get_collect_article_author = (userid) => {
  return request('get',`/user/user/${userid}`)
}
// 根据id取消用户的收藏
export const cancel_collect_by_id = (id) => {
  return request('delete',`/life/forum/collect/${id}`)
}

// 用户查看自己的留言记录
export const get_review_list = (pageSize, currentPage) => {
  return request('get',`/life/forum/review/list/${pageSize}/${currentPage}`)
}

// 用户删除自己的留言
export const delete_review_Byid = (id) => {
  return request('delete',`/life/forum/review/${id}`)
}
// 用户根据文章id删除自己的帖子
export const delete_my_article_ById = (id) => {
  return request('delete',`/life/forum/article/${id}`)
}

//    热搜总榜接口
export const get_forum_hot_topic = (pageSize, currentPage) => {
  return request('get',`/life/hot/total/${pageSize}/${currentPage}`)
}

//实时热搜
export const getForumCurrentHotTopic = (pageSize, currentPage) => {
  return request("get", `/life/hot/current/${pageSize}/${currentPage}`)
}


// 根据id获取帖子的所有信息
export const get_forum_article_all_message = (articleId) => {
  return request('get',`/life/forum/article/${articleId}`)
}
// 获得标签的分类列表
export const get_forum_tag_cate = (params) => {
  return request('get',`/life/forum/category`,params)
}
// 根据分类id获得其中的标签
export const get_forum_tag_by_cateId = (cateId) => {
  return request('get',`/life/forum/tag/list/${cateId}`)
}
// 获得当前用户的 获赞总数 文章总数 文章被浏览总次数 收藏总数 用户名  用户id
export const get_forum_My_message_index = () => {
  return request('get',`/life/forum/article/user`)
}
// 获取热门话题   接口使用的是实时总榜
export const get_forum_hot = () => {
  return request('get',`/life/hot`)
}
// 点赞
// 给文章点赞
export const up_article = (id) => {
  return request('get',`/life/forum/up/1/${id}`)
}
// 取消文章点赞
export const artcancel = (id) => {
  return request('post',`/life/forum/cancel/${id}/1/`)
}
// 给评论点赞
export const up_reply = (id) => {
  return request('get',`/life/forum/up/2/${id}`)
}

// 取消评论点赞
export const replycancel = (id) => {
  return request('get',`/life/forum/cancel/${id}/2/`)
}

// 根据id获得自己是否已经给文章点赞  1未点赞  2已经点赞
export const isUpArt = (id) => {
  return request('get',`/life/forum/state/1/${id}`)
}
// 根据一级留言id分页查询一级留言
export const get_replyMessage = (pageSize, currentPage, articleId) => {
  return request('get',`/life/forum/review/firstClass/${pageSize}/${currentPage}/${articleId}`)
}
// 根据一级留言获得其下留言信息  也是分页
export const get_replyMessage_All = (pageSize,currentPage,firstClassId) => {
  return request('get',`/life/forum/review/child/${pageSize}/${currentPage}/${firstClassId}`)
}
// 用户是否收藏了该文章  type直接设置为1
export const is_art_collect = (id) => {
  return request('get',`/life/forum/collect/state/${id}`)
}

//查看自己的转发记录
export const get_forward_list = (pageSize, currentPage) => {
  return request('get',`/life/forum/forward/list/${pageSize}/${currentPage}`)
}

//获得用户详细信息
export const user_detail = () => {
  return request("get", "/user/user/detail")
}

export const user_state = () => {
  return request("get", "/user/user/state")
}

//删除转发记录
export const delete_forward_by_id = (id) => {
  return request('delete',`/life/forum/forward/${id}`)
}

// 用户收藏文章
export const art_collect_ById= (id) => {
  return request('get',`/life/forum/collect/${id}`)
}

export const release_article = (articleInfo) => {
  return request("post", "/life/forum/article", articleInfo)
}

//用户获得自己的兴趣标签
export const getInterestOwner = () => {
  return request("get", "/user/interest/owner")
}


//获得所有的文章分类
export const getAllCategory = () => {
  return request("get", "/life/forum/category")
}

//用户更新头像
export const updateUserPortrait = (param) => {
  return request('post',`/user/user/portrait`, param)
}

export const getInterestTagByUserId = () => {
  return request("get", "/user/interest")
}

//修改用户信息
export const editUserInfo =  (userInfo) =>{
  return request("post", "/user/user/edit", userInfo)
}

//关注和取关兴趣标签  type:1-取关  2-关注
export const followInterestTag = (type ,id) => {
  return request('get',`/user/interest/follow/${type}/${id}`)
}

//获得论坛模块的总排名
export const getArticleRank = (pageSize, currentPage) => {
  return request("get", `life/forum/article/rank/${pageSize}/${currentPage}/`)
}

//根据用户id获得该用户在论坛中的简略信息
export const getUserForumBriefById = (userId) => {
  return request('get',`/life/forum/article/user/${userId}`)
}

//根据用户id获得用户的文章列表
export const getArticleListByUserId = (userId, pageSize, currentPage) => {
  return request("get", `/life/forum/article/user/${pageSize}/${currentPage}/${userId}`)
}

//删除已经上传的文件
export const remove_article_file = (fileParam) => {
  let form = new FormData();
  form.append("fileUri", fileParam)
  axios({
    method: 'delete',
    url: "/file/file",
    processData:false,
    contentType:false,
    data: form
  })
}

// 用户写评论
export const write_reply = (params) => {
  return request('post','/life/forum/review/write',params)
}


// 用户发表文章的时候选择标签



