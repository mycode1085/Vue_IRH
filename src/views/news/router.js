import Vue from 'vue'
import VueRouter from 'vue-router'
// import Header from '../../components/Header.vue'
// import Footer from '../../components/Footer.vue'
// import News_container from '../../components/news/news_container.vue'
import News_loading from '../../components/news/news_loading.vue'
import News_main from '../../components/news/news_main.vue'
import News_list from '../../components/news/news_list.vue'

Vue.component('newsloading',News_loading)

Vue.use(VueRouter)

const routes = [
  // {
  //   path:'/news',
  //   components:{
  //     header:Header,
  //     footer: Footer,
  //     news_main:News_main,
  //     news_loading: News_loading,
  //     news_list:News_list
  //   }
  // }
  {
    path:'/news',
    component:News_main,
    redirect: '/news_index',
    children: [
      {
        path: '/news_index',
        component:News_list
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
