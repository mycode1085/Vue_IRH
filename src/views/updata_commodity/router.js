import Vue from 'vue'
import VueRouter from 'vue-router'
import Header_simple from '../../components/Header_simple.vue'
import Footer from '../../components/Footer.vue'


// 导入主要的组件
import Updata_comm from '../../components/updata_comm_demand/updata_comm.vue'
import Updata_demand from '../../components/updata_comm_demand/updata_demand.vue'



Vue.use(VueRouter)
// Vue.components({
//   Header: Header_simple,
//   Footer:Footer
// })
Vue.component('Header',Header_simple)
Vue.component('Footer',Footer)

const routes = [
  {
    path:'/updata_commodity',
    component:Updata_comm
  },
  {
    path:'/updata_demand',
    component:Updata_demand
  }
 
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
