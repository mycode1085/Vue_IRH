// import Vue from 'vue'
import Vue from "@utils/index.js";  //单页面直接引用在外层main.js  多页面需每个html下js引入
import App from './App.vue'
import router from './router'
import store from '../../store/index.js'

// 导入全局样式表
import '../../assets/css/global.css'
// 导入element-UI
import '../../plugins/element'
// 导入iconfont字体样式
import '../../assets/css/font_iconfont/iconfont.css'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
