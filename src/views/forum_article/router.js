import Vue from 'vue'
import VueRouter from 'vue-router'
import Header from '../../components/Header.vue'
import Footer from '../../components/Footer'
import Forum_article from '../../components/forum_article/forum_article.vue'

Vue.use(VueRouter)

const routes = [
  {
    path:'/forum_article',
    components:{
      header:Header,
      footer: Footer,
      forum_article:Forum_article
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
