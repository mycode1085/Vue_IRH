import Vue from 'vue'
import VueRouter from 'vue-router'
import Header_simple from '../../components/Header_simple.vue'
import Footer from '../../components/Footer.vue'
// 导入个人信息面板
import OtherInfo from '../../components/othermess/otherInfo.vue'
// 导入文章列表
import articleList from '../../components/othermess/article_List.vue'
// 导入商品列表
import commodityList from '../../components/othermess/commodity_List.vue'
// 导入需求列表
import demandList from '../../components/othermess/demand_List.vue'

Vue.use(VueRouter)
// 全局使用组件
Vue.component('Header',Header_simple)
Vue.component('Footer',Footer)
Vue.component('OtherInfo',OtherInfo)
const routes = [
    {
      path: '/othermess',
      redirect: '/artList'
    },
    {
      path: '/artList',
      component:articleList
    },
    {
      path: '/commodityList',
      component:commodityList
    },
    {
      path: '/demandList',
      component:demandList
    }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
