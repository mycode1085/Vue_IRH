import Vue from 'vue'
import VueRouter from 'vue-router'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
// 导入内容区域
import Container from '../../components/search_content/container' 
import Recommend from '../../components/search_content/recommend' 

Vue.use(VueRouter)

const routes = [
  {
    path:'/search_content',
    components:{  
      header:Header,
      footer:Footer,
      container:Container,
      recommend:Recommend
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
