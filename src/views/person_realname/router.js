import Vue from 'vue'
import VueRouter from 'vue-router'

import Footer from '../../components/Footer.vue'
import Realname from '../../components/person_realname/real_name_detail.vue'
import Card from '../../components/person_realname/card.vue'
import Id_card from '../../components/person_realname/Id_card.vue'


Vue.use(VueRouter)
// 全局使用组件
Vue.component('Footer',Footer)
const routes = [
  {
    path: '/person_realname',
    components: {
      realname: Realname
    },
    children: [
      {
        path: '/',
        components: {
          'v-one-card': Card,
          'v-Id-card': Id_card
        }
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
