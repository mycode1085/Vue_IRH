import Vue from 'vue'
import VueRouter from 'vue-router'


import Header from '../../components/Header.vue'
import Footer from '../../components/Footer.vue'
import Banner from '../../components/index/banner.vue'
import Plate from '../../components/index/plate.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:'/index'
  },
  {
    path:'/index',
    components:{
      header: Header,
      footer: Footer,
      plate: Plate
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
