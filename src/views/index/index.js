// import Vue from 'vue'
import Vue from "@utils/index.js";  //单页面直接引用在外层main.js  多页面需每个html下js引入
import App from './App.vue'
import router from './router'
import store from '../../store/index.js'

// 导入全局样式表
import '../../assets/css/global.css'
// 导入element-UI
import '../../plugins/element.js'
// 导入iconfont字体样式
import '../../assets/css/font_iconfont/iconfont.css'
// 导入animate 动画样式
import '../../assets/css/animate/animate.css'
// 路由切换    显示加载进度条
import Nprogress from 'nprogress'
import 'nprogress/nprogress.css'

// 导入加密
let Base64 = require('js-base64').Base64
Vue.prototype.$base64 = Base64


Nprogress.configure({showSpinner:false})
router.beforeEach((to, form, next) => {
  Nprogress.start();
  next();
})
router.afterEach(() => {
  Nprogress.done();
})

Vue.config.productionTip = false

// 这个store是一个可以调取一个全局变量

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
