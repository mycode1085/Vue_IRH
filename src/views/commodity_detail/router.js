import Vue from 'vue'
import VueRouter from 'vue-router'
import HeaderSimple from '../../components/Header_simple.vue'
import Footer from '../../components/Footer.vue'
// 内容组件
import commodity_leaveMessage from '../../components/commodity_detail/leaveMessage.vue'
import commodity_detail from '../../components/commodity_detail/commodity_detail.vue'


Vue.component('header-simple',HeaderSimple) 
Vue.component('footer-c',Footer) 
Vue.component('commodityDetail',commodity_detail)
Vue.component('commodityleaveMessage',commodity_leaveMessage)
Vue.use(VueRouter)

const routes = [
  
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
