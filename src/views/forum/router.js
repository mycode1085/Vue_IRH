import Vue from 'vue'
import VueRouter from 'vue-router'
// import Header from '../../components/Header.vue'
// import Footer from '../../components/Footer.vue'
// 内容组件
import forum_main from '../../components/forum/forum_main.vue';
import hotTopic from '../../components/forum/hottopic/hottopic.vue'
import accumulate_hot from '../../components/forum/accumulate_hottopic/accumulate_hottoic.vue'
import index from '../../components/forum/indextopic/indextopic.vue'
import forum_loading from '../../components/forum/loading/forum_loading.vue'

Vue.component('forumloading',forum_loading)
// Vue.component('header',Header) 
// Vue.component('footer',Footer) 

Vue.use(VueRouter)

const routes = [
  {
    path:'/forum',
    component:forum_main,
    redirect: '/forum_index',
    children: [
      {
        path: '/forum_index',
        component:index
      },
      {
        path: '/hottopic',
        component:hotTopic
      },
      {
        path: '/accumulate_hot',
        component:accumulate_hot
      }

    ]
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
