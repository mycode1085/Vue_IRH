import Vue from 'vue'
import VueRouter from 'vue-router'
import Header from '../../components/Header.vue'
Vue.use(VueRouter)

const routes = [
  {
    path:'/about',
    components:{
      header:Header
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
