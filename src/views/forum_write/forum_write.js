// import Vue from 'vue'
import Vue from "@utils/index.js";  //单页面直接引用在外层main.js  多页面需每个html下js引入
import App from './App.vue'
import router from './router'
import store from '../../store/index.js'

// 导入全局样式表
import '../../assets/css/global.css'
// 导入element-UI
import '../../plugins/element'
// 导入iconfont 样式
import '../../assets/css/font_iconfont/iconfont.css'

import VueQuillEditor from 'vue-quill-editor'



// require styles  这个是quill富文本编辑器的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(VueQuillEditor)



// 路由切换    显示加载进度条
import Nprogress from 'nprogress'
import 'nprogress/nprogress.css'
Nprogress.configure({showSpinner:false})
router.beforeEach((to, form, next) => {
  Nprogress.start();
  next();
})
router.afterEach(() => {
  Nprogress.done();
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
