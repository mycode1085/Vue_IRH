import Vue from 'vue'
import VueRouter from 'vue-router'
import Header_simple from '../../components/Header_simple.vue'
import Footer from '../../components/Footer.vue'
import Write from '../../components/forum_write/write_article.vue'

Vue.use(VueRouter)

const routes = [
  {
    path:'/forum_write',
    components: {
      'v-header_simple':Header_simple,
      'v-footer': Footer,
      'v-write':Write
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
