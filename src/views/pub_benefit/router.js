import Vue from 'vue'
import VueRouter from 'vue-router'
import Header_simple from '../../components/Header_simple.vue'
import Footer from '../../components/Footer.vue'

import main_List_s from '../../components/pub_benefit/mainList_s.vue'
import main_List_i from '../../components/pub_benefit/mainList_i.vue'


Vue.use(VueRouter)

// 全局使用组件
Vue.component('Header',Header_simple)
Vue.component('Footer', Footer)

const routes = [
  {
    path: '/pub_benefit',
    components: {
      main_List_s,
      main_List_i
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})
export default router