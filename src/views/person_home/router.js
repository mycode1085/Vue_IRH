import Vue from 'vue'
import VueRouter from 'vue-router'
import Header_simple from '../../components/Header_simple.vue'
import Footer from '../../components/Footer.vue'
import Forum_home_my from '../../components/forum_home/forum_home_my.vue'
import Forum_home_collection from '../../components/forum_home/forum_home_collection.vue'
import Forum_home_transmit from '../../components/forum_home/forum_home_transmit'
import Forum_home_reply from '../../components/forum_home/forum_home_reply.vue'
import Forum_home_tag from '../../components/forum_home/forum_home_tag.vue'
import Forum_home_article from '../../components/forum_home/forum_home_article.vue'

// 导入商城管理的组件
import Store_home_demand from '../../components/store_home/store_home_demand.vue'
import Store_home_evaluate from '../../components/store_home/store_home_evaluate.vue'
import Store_home_goods from '../../components/store_home/store_home_goods.vue'
import Store_home_leavemessage from '../../components/store_home/store_home_leavemessage.vue'
import Store_home_orders from '../../components/store_home/store_home_orders.vue'


Vue.use(VueRouter)
// 全局使用组件
Vue.component('Header',Header_simple)
Vue.component('Footer',Footer)
const routes = [{
    path: '/person_home',
    redirect:'/forum_home_my'
  },
  {
    path: '/forum_home_my',
    component:Forum_home_my
  },
  {
    path: '/forum_home_collection',
    component:Forum_home_collection
  },
  {
    path: '/forum_home_transmit',
    component:Forum_home_transmit
  },
  {
    path: '/forum_home_reply',
    component:Forum_home_reply
  },
  {
    path: '/forum_home_tag',
    component:Forum_home_tag
  },
  {
    path: '/forum_home_article',
    component:Forum_home_article
  },
  {
    path: '/store_home_demand',
    component:Store_home_demand
  },
  {
    path: '/store_home_evaluate',
    component:Store_home_evaluate
  },
  {
    path: '/store_home_goods',
    component:Store_home_goods
  },
  {
    path: '/store_home_leavemessage',
    component:Store_home_leavemessage
  },
  {
    path: '/store_home_orders',
    component:Store_home_orders
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
