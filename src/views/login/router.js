import Vue from 'vue'
import VueRouter from 'vue-router'
import Header_simple from '../../components/Header_simple.vue'
import Footer from '../../components/Footer.vue'

import Input from '../../components/login/login_input.vue'
import code_input from '../../components/login/code_input.vue'

Vue.use(VueRouter)
// 全局使用组件
Vue.component('Header',Header_simple)
Vue.component('Footer',Footer)
const routes = [{
    path: '/login',
    components: {
      Input,code_input
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
