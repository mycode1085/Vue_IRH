import Vue from 'vue'
import VueRouter from 'vue-router'
import upload_Information from '../../components/register/upload_information'
import upload_materials from '../../components/register/upload_materials'

import wait_review from '../../components/register/wait_review'

Vue.use(VueRouter)

const routes = [
  {
    path:'/register',
    components:{
      information:upload_Information,
      upload_materials:upload_materials,
      wait_review:wait_review
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
