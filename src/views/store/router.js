import Vue from 'vue'
import VueRouter from 'vue-router'
import Header from '../../components/Header.vue'
import Banner from '../../components/store/Banner.vue'
import Scroll from '../../components/store/index_right_scroll.vue'
import Person_center from '../../components/store/person_center.vue'
import Footer from '../../components/Footer.vue'
// import SideBar from '../../components/Sidebar'
import Register from '../../components/store/register'
import intelligent_recommendation from '../../components/store/intelligent_recommendation'
import demand_recommendation from '../../components/store/demand_recommendation'



Vue.use(VueRouter)

const routes = [
  {
    path:'/store',
    components:{
      header:Header,
      banner:Banner,
      scroll:Scroll,
      person_center:Person_center,
      footer:Footer,
      intelligent_recommendation,
      demand_recommendation:demand_recommendation
    },
    children:[{
      path:'/register',
      components:{
       register:Register
      }
    }]
  }
 
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
