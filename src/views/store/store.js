// import Vue from 'vue'
import Vue from "@utils/index.js";  //单页面直接引用在外层main.js  多页面需每个html下js引入
import App from './App.vue'
import router from './router'
import store from '../../store/index.js'
// 导入全局css
import '../../assets/css/global.css'
import '../../plugins/element'
// 导入字体图标
import '../../assets/css/font-awesome-4.7.0/css/font-awesome.min.css'
import '../../assets/css/font_iconfont/iconfont.css'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
