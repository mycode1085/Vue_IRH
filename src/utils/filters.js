export function amount(n) {
	n = (n / 100).toFloor(2);
	var num = String(n).split(".");
	return (
		num[0]
			.split("")
			.reverse()
			.map((item, index, array) => {
				if (++index !== array.length && index % 3 === 0) {
					return "," + item;
				}
				return item;
			})
			.reverse()
			.join("") + (num[1] ? "." + num[1] : "")
	);
}
export function dateFormat (date,arg) {
	var time=new Date(date);
	var fmt= arg || 'yyyy-MM-dd';
	var o = {
		"M+": time.getMonth() + 1, //月份
		"d+": time.getDate(), //日
		"h+": time.getHours(), //小时
		"m+": time.getMinutes(), //分
		"s+": time.getSeconds(), //秒
		"q+": Math.floor((time.getMonth() + 3) / 3), //季度
		"S": time.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (time.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o)
		if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}
export function timeago (time) {  
  
  // 传进来的 time  "2020-02-24 09:35:00";
  // 将time转化为一个时间毫秒，注意时间戳是秒的形式，在这个毫秒的基础上除以1000，就是十位数的时间戳。13位数的都是时间毫秒。
  var stringTime = time;
  var dateTimeStamp = new Date(stringTime).getTime();
    var minute=1000*60;      //把分，时，天，周，半个月，一个月用毫秒表示
    var  hour=minute*60;
    var day=hour*24;
    var week=day*7;
    var month = day * 30;
    let result;

    var  now=new Date().getTime();   //获取当前时间毫秒
    var diffValue=now - dateTimeStamp;//时间差
  
    if(diffValue<0){return;}

    var  minC=diffValue / minute;  //计算时间差的分，时，天，周，月
    var  hourC=diffValue / hour;
    var  dayC=diffValue / day;
    var  weekC=diffValue / week;     
    var  monthC=diffValue / month;

    if(monthC>=1){
      result="" + parseInt(monthC) + "月前";
    }
    else if(weekC>=1){
      result="" + parseInt(weekC) + "周前";
    }
    else if(dayC>=1){
      result=""+ parseInt(dayC) +"天前";
    }
    else if(hourC>=1){
      result=""+ parseInt(hourC) +"小时前";
    }
    else if(minC>=1){
      result=""+ parseInt(minC) +"分钟前";
    }else
    result="刚刚";
    return result;
}
// 数字过滤器   大于1000 以k为单位 大于10000 以 为单位  保留两位小数
export function numberFormat (num) {
  var result;
  if (num<1000){
    result = num
    return result
  }
  else if (num >= 10000) {
    result =  (num/10000).toFixed(2)+ "w"
  }
  else if (num >= 1000) {
    result =  (num/1000).toFixed(2) + "k"
  }
  
  return result
}


export function timeSplit(time,i) {
  let t1 = time.split(' ')
  if (i === 'day') {
    return t1[0]
  }
  if (i === 'second') {
    return t1[1]
  }
}
// 用户身份过滤器
// 10:普通会员 20:服务人员 30:校园组织 35:公益组织 40:校园社团 50:管理员
export function identityfilter (num) {
  let inden = '';
  switch (num) {
    case 10:
      inden = '普通会员'
      break;
    case 20:
      inden = '服务人员'
      break;
    case 30:
      inden = '校园组织'
      break;
    case 35:
      inden = '公益组织'
      break;
    case 40:
      inden = '校园社团'
      break;
    case 50:
      inden = '管理员'
      break;
    default:
      inden = '其他'
      break;
  }
  return inden;
}
// 用户认证状态过滤器   
// 10:注销 20:锁定 25:未实名 30:审核中 40:审核通过 50:认证失败
export function statefilter(num) {
  let state = '';
  switch (num) {
    case 10:
      state = '注销'
      break;
    case 20:
      state = '锁定'
      break;
    case 25:
      state = '未实名'
      break;
    case 30:
      state = '审核中'
      break;
    case 40:
      state = '审核通过'
      break;
    case 50:
      state = '认证失败'
      break;
    default:
      state = '其他'
      break;
  }
  return state;
}
// 用户性别过滤器
// 1 女  2 男  3  保密
export function genderfilter(num){
  let gender = '保密';
  switch (num) {
    case 1:
      gender = '女'
      break;
    case 2:
      gender = '男'
      break;
    case 3:
      gender = '保密'
      break;
    default:
      gender = '其他'
      break;
  }
  return gender;
}

// 时间过滤器  只要年月日 不要时分秒
export function timefilter(str){
  // 使用空格将时间分开成数组
  return str.split(' ')[0]
}