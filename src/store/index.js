import Vue from 'vue'
import Vuex from 'vuex'
// 设置一个全局变量


Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    // 加载图片 基地址
    imgURL: 'https://www.imuster.top/img/',
    // 上传文件夹的 url
    uploadImgURL:'/file/file'
  },
  // 这里设置方法修改 state中的变量
  mutations:{
    
  },
  // 这里是支持异步的触发mutations  从而达到异步修改state中的值
  actions:{
    
  }
})